# fastp Singularity container
### Bionformatics package fastp<br>
A FASTQ preprocessor with full features (QC/adapters/trimming/filtering/splitting...)<br>
fastp Version: 0.20.1<br>
[https://github.com/OpenGene/fastp]

Singularity container based on the recipe: Singularity.fastp_v0.20.1

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build fastp_v0.20.1.sif Singularity.fastp_v0.20.1`

### Get image help
`singularity run-help ./fastp_v0.20.1.sif`

#### Default runscript: STAR
#### Usage:
  `fastp_v0.20.1.sif --help`<br>
    or:<br>
  `singularity exec fastp_v0.20.1.sif fastp --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull fastp_v0.20.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/fastp/fastp:latest`


